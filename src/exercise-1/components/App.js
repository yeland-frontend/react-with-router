import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router} from 'react-router-dom';
import Header from "./Header";
import {Route, Switch} from "react-router";
import Home from "./Home";
import MyProfile from "./MyProfile";
import AboutUs from "./AboutUs";
import Products from "./Products";
import ProductDetails from "./ProductDetails";

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <div>
            <Header/>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/products" component={Products}/>
              <Route exact path="/goods" component={Products}/>
              <Route path="/my-profile" component={MyProfile}/>
              <Route path="/about-us" component={AboutUs}/>
              <Route path="/products/:id" component={ProductDetails}/>
              <Route component={Home}/>
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
