import React from "react";
import {NavLink} from "react-router-dom";

const Header = () => {
  return (
    <nav>
      <ul>
        <li>
          <NavLink to="/" exact activeClassName="activeLink">Home</NavLink>
        </li>
        <li>
          <NavLink to="/products" activeClassName="activeLink">Products</NavLink>
        </li>
        <li>
          <NavLink to="/my-profile" activeClassName="activeLink">My Profile</NavLink>
        </li>
        <li>
          <NavLink to="/about-us" activeClassName="activeLink">About Us</NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Header;