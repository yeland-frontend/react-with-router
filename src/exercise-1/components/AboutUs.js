import React from "react";
import {Link} from "react-router-dom";

const AboutUs = () => {
  return (
    <div className="content">
      <p >Company: ThoughtWorks Local</p>
      <p>Location: Xi'an</p>
      <p></p>
      <p>For more information, please</p>
      <p className="website">view out <Link to="/" >website</Link></p>
    </div>
  )
}

export default AboutUs;