import React from "react";
import {Link} from "react-router-dom";

const Products = () => {
  return (
    <div className="content product">
      <p>All Products</p>
      <Link className="item" to="/products/1">Bicycle</Link>
      <Link className="item" to="/products/2">TV</Link>
      <Link className="item" to="/products/3">Pencil</Link>
    </div>
  )
};

export default Products;