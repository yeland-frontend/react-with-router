import React from "react";
const data = [
  {
    "id": 1,
    "name": "Bicycle",
    "price": 30,
    "quantity": 15,
    "desc": "Bicycle is Good"
  },
  {
    "id": 2,
    "name": "TV",
    "price": 40,
    "quantity": 16,
    "desc": "TV is good"
  },
  {
    "id": 3,
    "name": "Pencil",
    "price": 50,
    "quantity": 17,
    "desc": "Pencil is good"
  }
];


const ProductDetails = (props) => {
  const id = props.match.params.id;
  const product = data[id-1];
  return (
    <div className="content">
      <p>Product Details:</p>
      <p>Name: {product.name}</p>
      <p>Id: {product.id}</p>
      <p>Price: {product.price}</p>
      <p>Quantity: {product.quantity}</p>
      <p>Desc: {product.desc}</p>
      <p>URL:/products/{id}</p>
    </div>
  )
};

export default ProductDetails;