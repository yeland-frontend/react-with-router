import React from 'react';
import {Redirect} from "react-router";

const User = (props) => {
  return (
    isNaN(props.match.params.user) ?
      <div>
        404 Not Found.
      </div>
      : <div>
        User profile page.
      </div>
  );


};

export default User;